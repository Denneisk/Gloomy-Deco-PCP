/**
	Copyright 2021 Denneisk

	This file is part of Gloomy Deco PCP.

    Gloomy Deco PCP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gloomy Deco PCP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gloomy Deco PCP.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using RimWorld;
using Verse;
using System.Reflection;
using System.Collections;
using System.Linq;
using System.Reflection.Emit;
using HarmonyLib;
using System.Collections.Generic;
using UnityEngine;

namespace GloomyPCP
{

	public static class Boat
	{

		public static void RemoveBoat()
		{

			ThingDef BoatThing = DefDatabase<ThingDef>.AllDefs.Last((ThingDef t) => t.defName == "GloomyDeco_Boat");

			if (BoatThing != null)
			{
				if(GloomyDecoPCPSettings.debug)
					Log.Message("Removing Boat class");
				BoatThing.thingClass = typeof(Building);
			}
		}

		public static void ReplaceBoat()
		{

			ThingDef BoatThing = DefDatabase<ThingDef>.AllDefs.Last((ThingDef t) => t.defName == "GloomyDeco_Boat");

			if (BoatThing != null)
			{
				if(GloomyDecoPCPSettings.debug)
					Log.Message("Replacing Boat class");
				BoatThing.thingClass = typeof(BoatReplacement);
			}
		}

		public static void PatchBoat(Harmony harmony)
		{
			//Log.Message("Getting original");
			MethodInfo original = AccessTools.Method("GloomyDeco.GloomyDeco_Boat:Tick", null, null);
			//Log.Message("Getting my method");
			MethodInfo myMethod_Boat = typeof(Boat).GetMethod("Tick_ReversePatch");
			ReversePatcher rp = harmony.CreateReversePatcher(original,new HarmonyMethod(myMethod_Boat));
			rp.Patch(HarmonyReversePatchType.Original);
			
		}//PatchBoat

		public static void Tick_ReversePatch(object __instance)
		{
			IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        	{
            	var list = new List<CodeInstruction>(instructions);

				//Find the bad method call
            	int idx = list.FindIndex(item => item.Calls(Patch.VoidMethod));
            	//Replace the previous argument and remove original call
				list[idx] = CodeInstruction.Call(typeof(FleckMaker), "WaterSplash");

				idx = list.FindIndex(idx+1, item => item.Calls(Patch.VoidMethod));
				list[idx] = CodeInstruction.Call(typeof(FleckMaker), "WaterSplash");

            	return list;
        	}//transpiler
			
			_ = Transpiler(null);

			throw new NotImplementedException();
		}

		public class BoatReplacement : Building
		{
			private Vector3 loc = default(Vector3);
			private float f = Rand.Range(0f, Mathf.PI*2f);
			private int splash = 0;

			public override void Tick()
			{
				//Unfortunate case of this not being static
				Tick_ReversePatch(this);
			}

			public override void Draw()
			{
				this.DrawAt(this.DrawPos + this.loc, false);
			}

			
		}

		
	}

}