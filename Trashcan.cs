/**
	Copyright 2021 Denneisk

	This file is part of Gloomy Deco PCP.

    Gloomy Deco PCP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gloomy Deco PCP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gloomy Deco PCP.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using RimWorld;
using Verse;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using System.Reflection;
using System.Reflection.Emit;

namespace GloomyPCP
{
	public static class Trashcan
	{
		static ThingDef TrashCanThing = DefDatabase<ThingDef>.AllDefs.Last((ThingDef t) => t.defName == "GloomyDeco_TrashCan");

		public static void RemoveTrashcanComps()
		{

			if (TrashCanThing != null)
			{
				CompProperties comp;
				try{
					comp = TrashCanThing.comps.Last((CompProperties c) => c.compClass == AccessTools.TypeByName("GloomyDeco.GloomyDeco_TrashCanComp"));
				}
				catch(Exception)
				{
					comp = null;
					Log.Warning("Gloomy Deco PCP: Did not find TrashCanComp in TrashCan Def.\nThis should not happen.");
				}
				if(comp != null)
				{
					if(GloomyDecoPCPSettings.debug)
						Log.Message("Removing old TrashCanComp");
					TrashCanThing.comps.Remove(comp);
				}
			}
		}

		public static void ReplaceTrashcanComps()
		{
			if (TrashCanThing != null)
			{

				if(GloomyDecoPCPSettings.debug)
					Log.Message("Adding new TrashCanComp");
				TrashCanThing.comps.Add(new CompProperties(typeof(ReplacementTrashcan)));
			}
		}

		public static void PatchTrashCan(Harmony harmony)
		{
			//Log.Message("Getting original");
			MethodInfo original = AccessTools.Method("GloomyDeco.HarmonyPatches_MakeFilth:Prefix", null, null);
			//Log.Message("Getting my method");
			MethodInfo myMethod_Trashcan = typeof(Trashcan).GetMethod("MakeFilth_Prefix_ReversePatch");
			ReversePatcher rp = harmony.CreateReversePatcher(original,new HarmonyMethod(myMethod_Trashcan));
			rp.Patch(HarmonyReversePatchType.Original);

			Trashcan.ReplaceTrashcanComps();

			//Use fixed TryMakeFilth patch
			harmony.Patch(
				AccessTools.Method(
					"RimWorld.FilthMaker:TryMakeFilth",
					new Type[]{typeof(IntVec3), typeof(Map), typeof(ThingDef), typeof(IEnumerable<string>), typeof(bool), typeof(RimWorld.FilthSourceFlags)}),
				prefix: new HarmonyMethod(myMethod_Trashcan)
			);
				

			HarmonyMethod tryGetCompFix = new HarmonyMethod(typeof(Trashcan).GetMethod("Generic_TryGetComp_TranspilerPatch"));
			//Patch HasJobOnThing to use new comp
			harmony.Patch(AccessTools.Method("GloomyDeco.GloomyDeco_WorkGiver_CleaningTrashCan:HasJobOnThing"), transpiler: tryGetCompFix);

			//Funny thing about this: because Clean() is functionally equivalent in both version, you don't need to do this for that method (I believe).
			//However, because the two NeedCleaning methods are functionally different, this is required.
			//If this isn't done, cleaners won't clean trash automatically.
			harmony.Patch(AccessTools.Method("GloomyDeco.GloomyDeco_WorkGiver_CleaningTrashCan:HasJobOnThing"),
							transpiler: new HarmonyMethod(typeof(Trashcan).GetMethod("Generic_NeedCleaning_TranspilerPatch")));
				
			//Patch MakeNewToils delegate to use new comp
			//For some reason getting the nested type is not possible (or I'm just dumb) with AccessTools.Method, so I had to do this little thing
			MethodInfo cleaningTrashCan = AccessTools.Method(AccessTools.TypeByName("GloomyDeco.GloomyDeco_JobDriver_CleaningTrashCan")
											.GetNestedType("<>c__DisplayClass1_0", BindingFlags.NonPublic),"<MakeNewToils>b__0");
			harmony.Patch(cleaningTrashCan, transpiler: tryGetCompFix);

			var gloomyGSTPatch = AccessTools.Method("GloomyDeco.HarmonyPatches_GetStatValue:Postfix");
			harmony.Patch(gloomyGSTPatch, transpiler:tryGetCompFix);
			
			if (GloomyDecoPCPSettings.useAlternativeTrashBeauty)
			{
				harmony.Patch(gloomyGSTPatch, transpiler:new HarmonyMethod(typeof(Trashcan).GetMethod("Replace_Minus30_With_Custom")));
			}

			harmony.Patch(typeof(StatExtension).GetMethod("GetStatValue"), postfix:new HarmonyMethod(gloomyGSTPatch));

		}//PatchTrashCan

		public static bool MakeFilth_Prefix_ReversePatch(IntVec3 c, Map map, ThingDef filthDef)
		{
			IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        	{
            	var list = new List<CodeInstruction>(instructions);

				//Find the bad method call
            	int idx = list.FindIndex(item => item.Calls(Patch.VoidMethod));
            	//Replace the previous argument and remove original call
				list[idx-1] = CodeInstruction.Call(typeof(GridsUtility),"GetRoom", null, null);
				list.RemoveAt(idx);
				
				idx = list.FindIndex(item => item.Calls(AccessTools.Method(typeof(ThingCompUtility), "TryGetComp", null, new Type[]{AccessTools.TypeByName("GloomyDeco_TrashCanComp")})));
				list[idx] = CodeInstruction.Call(typeof(ThingCompUtility),"TryGetComp", null, new Type[]{typeof(Trashcan.ReplacementTrashcan)});
				
				idx = list.FindIndex(item => item.Calls(AccessTools.Method("GloomyDeco.GloomyDeco_TrashCanComp:CanInput", null, null)));
				list[idx] = CodeInstruction.Call(typeof(Trashcan.ReplacementTrashcan),"get_IsFull", null, null);

				idx = list.FindIndex(item => item.Calls(AccessTools.Method("GloomyDeco.GloomyDeco_TrashCanComp:Input", null, null)));
				list[idx] = CodeInstruction.Call(typeof(Trashcan.ReplacementTrashcan),"Input", null, null);

				/*
				//Testing return because the trash can confused me
				idx = list.FindIndex(item => item.opcode == OpCodes.Ret);
				list.Insert(idx, new CodeInstruction(OpCodes.Ldloc_S, 9));
				list.Insert(idx+1, CodeInstruction.Call("System.Convert:ToString",new Type[]{typeof(bool)},null));
				list.Insert(idx+2, CodeInstruction.Call("Verse.Log:Message",new Type[]{typeof(string)},null));
				*/
            	return list;
        	}//transpiler
			
			_ = Transpiler(null);

			throw new NotImplementedException();
		}

		public static IEnumerable<CodeInstruction> Generic_TryGetComp_TranspilerPatch(IEnumerable<CodeInstruction> instructions)
        {
			//Maybe just make a single thing that uses expression IDK
         	var list = new List<CodeInstruction>(instructions);

			int idx;

			while(
					( idx = list.FindIndex(
						item => item.Calls(AccessTools.Method(typeof(ThingCompUtility), "TryGetComp", null, new Type[]{AccessTools.TypeByName("GloomyDeco_TrashCanComp")})))
					) != -1)	//This mess asks if idx still points to something valid
				list[idx] = CodeInstruction.Call(typeof(ThingCompUtility),"TryGetComp", null, new Type[]{typeof(Trashcan.ReplacementTrashcan)});
			
        	return list;
        }

		public static IEnumerable<CodeInstruction> Generic_NeedCleaning_TranspilerPatch(IEnumerable<CodeInstruction> instructions)
        {
         	var list = new List<CodeInstruction>(instructions);
			
			int idx;

			while(
					( idx = list.FindIndex(item => item.Calls(AccessTools.Method("GloomyDeco.GloomyDeco_TrashCanComp:get_NeedCleaning"))) ) != -1
				)
				list[idx] = CodeInstruction.Call(typeof(Trashcan.ReplacementTrashcan),"get_NeedCleaning");
			
        	return list;
        }

		public static IEnumerable<CodeInstruction> Replace_Minus30_With_Custom(IEnumerable<CodeInstruction> instructions)
		{
			var list = new List<CodeInstruction>(instructions);
			
			int idx;

			while((idx = list.FindIndex(item => item.Is(OpCodes.Ldc_R4,-30f))) != -1)
				list[idx] = CodeInstruction.LoadField(typeof(GloomyDecoPCPSettings),"alternativeBeautyModifier");
			
        	return list;
		}

		//[StaticConstructorOnStartup]
		public class ReplacementTrashcan : ThingComp
		{
			private static Material FuelBarFill = (Material)AccessTools.Field(AccessTools.TypeByName("GloomyDeco_TrashCanComp"),"FuelBarFilledMat").GetValue(null);
			private static Material FuelBarUnfill = (Material)AccessTools.Field(AccessTools.TypeByName("GloomyDeco_TrashCanComp"),"FuelBarUnfilledMat").GetValue(null);
			private Material material;
			private int trash = 0;

			/*public ReplacementTrashcan()
			{
				material = GraphicDatabase.Get<Graphic_Single>("GloomyDeco/TrashCanFull", ShaderDatabase.CutoutComplex, Vector2.one, parent.DrawColor).MatAt(parent.Rotation, null);
			}*/

			/*
			public override void Initialize(CompProperties props)
			{
				this.props = props;
			}*/

			public bool NeedCleaning
			{
				get
				{
					return trash >= GloomyDecoPCPSettings.trashCleanLimit;
				}
			}//NeedCleaning

			public bool IsFull
			{
				get
				{
					return trash >= GloomyDecoPCPSettings.trashLimit;
				}
			}

			public void Input() => trash++;

			public void Clean() => trash = 0;

			public override void PostExposeData()
			{
				base.PostExposeData();
				Scribe_Values.Look<int>(ref trash, "trash", 0, false);
			}

			public override void PostDraw()
			{
				base.PostDraw();

				//I do not understand why this is necessary
				if(material == null)
				{
					material = GraphicDatabase.Get<Graphic_Single>("GloomyDeco/TrashCanFull", ShaderDatabase.CutoutComplex, Vector2.one, parent.DrawColor).MatAt(parent.Rotation, null);
				}

				if (IsFull)
				{
					Graphics.DrawMesh(MeshPool.plane10, parent.DrawPos + new Vector3(0f, 0.01f, 0f), this.parent.Rotation.AsQuat, material, 0);
				}

				if (GloomyDecoPCPSettings.drawTrashBar)
				{
					GenDraw.FillableBarRequest trashBar = new GenDraw.FillableBarRequest
					{
						center = parent.DrawPos + new Vector3(0f, 0.05f, -0.3f),
						size = new Vector2(0.6f, 0.06f),
						fillPercent = (float)trash / GloomyDecoPCPSettings.trashLimit,
						filledMat = FuelBarFill,
						unfilledMat = FuelBarUnfill,
						margin = 0.15f,
						rotation = Rot4.East.Rotated(RotationDirection.Counterclockwise)
					};

					GenDraw.DrawFillableBar(trashBar);
				}
			}//PostDraw

			public override string CompInspectStringExtra()
			{
				return "GloomyDeco_TrashCan_Filth".Translate() + ": " + this.trash;
			}

		}
	}
}