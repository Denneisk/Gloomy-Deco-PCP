/**
	Copyright 2021 Denneisk

	This file is part of Gloomy Deco PCP.

    Gloomy Deco PCP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gloomy Deco PCP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gloomy Deco PCP.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using HarmonyLib;
using RimWorld;
using Verse;
using System.Reflection;
using Microsoft.CSharp;

namespace GloomyPCP
{
	public class Patch
	{

		///<summary>Represents the <c>MethodInfo</c> for a dummy method to be replaced.</summary>
		public static MethodInfo VoidMethod = typeof(Patch).GetMethod("voidMethod");
		public static void patch()
		{
			Harmony harmony = new Harmony("com.rimworld.denneisk.pcp.gloomy.deco");
			//harmony.UnpatchAll("com.rimworld.Dalrae.GloomyDeco");

			MethodInfo originalHarmony = AccessTools.Method("HarmonyLib.MethodBodyReader:ReadOperand",null, null);
			
			harmony.Patch(originalHarmony, finalizer: new HarmonyMethod(AccessTools.Method("GloomyPCP.Patch:harmony_finalizer",null,null)));//We do a little trolling

			//Unpatch bad TryMakeFilth
			harmony.Unpatch(AccessTools.Method(
					"RimWorld.FilthMaker:TryMakeFilth",
					new Type[]{typeof(IntVec3), typeof(Map), typeof(ThingDef), typeof(IEnumerable<string>), typeof(bool), typeof(RimWorld.FilthSourceFlags)}),
				HarmonyPatchType.Prefix,"com.rimworld.Dalrae.GloomyDeco");

			patchDeco(harmony);

			harmony.Unpatch(originalHarmony, HarmonyPatchType.All, "com.rimworld.denneisk.pcp.gloomy.deco");
		}

		public static void patchDeco(Harmony harmony)
		{
			Trashcan.RemoveTrashcanComps();
			//Unpatch Gloomy Deco broken patch
			harmony.Unpatch(typeof(StatExtension).GetMethod("GetStatValue"), HarmonyPatchType.Postfix, "com.rimworld.Dalrae.GloomyDeco");
			
			if (GloomyDecoPCPSettings.replaceTrashCanComp)
				Trashcan.PatchTrashCan(harmony);


			Boat.RemoveBoat();
			if (GloomyDecoPCPSettings.replaceBoat)
			{
				Boat.PatchBoat(harmony);
				Boat.ReplaceBoat();
			}
			else
			{	
				harmony.Unpatch(AccessTools.Method("GloomyDeco.HarmonyPatches_TryGainMemoryFast:Prefix"), HarmonyPatchType.Prefix, "com.rimworld.Dalrae.GloomyDeco");
			}
				
			
		}

		//The point of this patch is to make Harmony fill in a dummy method when a nonexistant method is read.
		//The default behaviour would be to throw an exception that I cannot easily work around.
		static Exception harmony_finalizer(Exception __exception, ref object instruction)
		{
			//Log.Message("Finalizer called");
			if(__exception is null)
				return null;

  			if (!(__exception is MissingMethodException))
			{
				return __exception;
			}

			//Log.Message("Finalizer doing accesstools");
			Type iltype = AccessTools.TypeByName("HarmonyLib.ILInstruction");

			//dynamic ilins = Convert.ChangeType(instruction,iltype);
			FieldInfo ilins_operand = AccessTools.Field(iltype,"operand");
			FieldInfo ilins_argument = AccessTools.Field(iltype,"argument");

			try
			{
				//Replace the bad method with a method that should be replaced.
				MethodInfo dynamicMethod = VoidMethod;
				
				//ilins.operand = dynamicMethod;
				//ilins.argument = (MethodInfo)ilins.operand;
				ilins_operand.SetValue(instruction,(MethodBase)dynamicMethod);
				ilins_argument.SetValue(instruction,dynamicMethod);
  				return null;
			}
			catch(Exception e)
			{
				return e;
			}
		}


		//Due to C# peepeepoopoo, this isn't very feasible
		/*
		public static IEnumerable<CodeInstruction> MakeFilth_Prefix_Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			List<CodeInstruction> list = new List<CodeInstruction>(instructions);
			int i = 0;
			//bool found = false;

			for (;i < list.Count;i++)
			{
				if(list[i].opcode == OpCodes.Call && list[i].OperandIs((MethodBase)AccessTools.Method("Verse.GridsUtility:GetRoom", null, null)))
				{
					list[i-1] = CodeInstruction.Call(typeof(GridsUtility),"GetRoom", null, null);
					list[i] = new CodeInstruction(OpCodes.Nop, null);
					break;
				}
			}//for

			return list;
		}
		*/

		public static void voidMethod()
		{
			throw new MissingMethodException("Unpatched broken method");
		}
	}
}