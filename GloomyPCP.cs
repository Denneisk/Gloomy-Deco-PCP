﻿/**
	Copyright 2021 Denneisk

	This file is part of Gloomy Deco PCP.

    Gloomy Deco PCP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gloomy Deco PCP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gloomy Deco PCP.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using RimWorld;
using Verse;
using UnityEngine;

namespace GloomyPCP
{
	[StaticConstructorOnStartup]
	public class GloomyDecoPCP : Mod
	{

		GloomyDecoPCPSettings settings;

		public GloomyDecoPCP(ModContentPack content) : base(content)
		{
			this.settings = GetSettings<GloomyDecoPCPSettings>();

			LongEventHandler.QueueLongEvent(new Action(Patch.patch), "GloomyDecoPCP.Patch", false, null, true);
			//Patch.patch();
		}

		public override void DoSettingsWindowContents(Rect inRect)
        {
				GloomyDecoPCPSettings.DrawSettings(inRect);
		}

		public override string SettingsCategory()
   		{
			return "Gloomy Deco PCP";
		}
	}

	
}
