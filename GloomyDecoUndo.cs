using System;
using RimWorld;
using Verse;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;
using HarmonyLib;

namespace GloomyPCP
{
	public static class GloomyDecoUndo
	{

		public static void UndoGloomyDeco(Harmony harmony)
		{

			List<ThingDef> things = new List<ThingDef>(DefDatabase<ThingDef>.AllDefs.Where(t => t.defName.StartsWith("GloomyDeco_")));
			for(int i = 0; i < things.Count; i++)
			{
				List<CompProperties> comps = new List<CompProperties>(things[i].comps.Where(cp => cp.compClass.Name.StartsWith("GloomyDeco_")));
			}

			/*
			//MethodInfo gloomyDecoConstructor = AccessTools.Method("GloomyDeco.GloomyDeco:.ctor",null, null);
			MethodInfo gloomyDecoConstructor = AccessTools.TypeByName("GloomyDeco.GloomyDeco").GetMethod(".ctor",BindingFlags.Static);

			Log.Message(gloomyDecoConstructor.ToStringSafe());

			MethodInfo newConstructor = harmony.Patch(gloomyDecoConstructor, null,null,new HarmonyMethod(typeof(GloomyDecoUndo).GetMethod("GloomyDecoTranspiler")), null);
			Log.Message("Undoing GloomyDeco constructor");
			newConstructor.Invoke(null, null);
			*/
			
		}

		//On second thought, maybe I am overengineering this
		/*
		public static IEnumerable<CodeInstruction> GloomyDecoTranspiler(IEnumerable<CodeInstruction> instructions)
		{

			List<CodeInstruction> list = new List<CodeInstruction>(instructions);

			//Remove the Harmony init at the beginning
			for(int i = 0; i < list.Count; i++)
			{
				if(list[i].Calls(typeof(Harmony).GetMethod("PatchAll")))
				{
					list.RemoveRange(0, i+1);
					break;
				}
			}

			//Replace Add with Remove :^)
			for(int i=0; i < list.Count; i++)
			{
				if(list[i].Calls(typeof(IEnumerable<ThingDef>).GetMethod("Add")))
				{
					list[i] = CodeInstruction.Call(typeof(IEnumerable<ThingDef>),"RemoveAll",null, null);
					break;
				}
			}

			return list;
		}//GloomyDecoTranspiler
		*/
	}
}