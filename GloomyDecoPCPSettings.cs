/**
	Copyright 2021 Denneisk

	This file is part of Gloomy Deco PCP.

    Gloomy Deco PCP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gloomy Deco PCP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gloomy Deco PCP.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using RimWorld;
using Verse;
using UnityEngine;

namespace GloomyPCP
{
	[StaticConstructorOnStartup]
	public class GloomyDecoPCPSettings : ModSettings
	{

		public static bool replaceTrashCanComp = true;
		public static int trashCleanLimit = 8;
		public static int trashLimit = 10;
		public static bool drawTrashBar = true;
		public static bool useAlternativeTrashBeauty = false;
		public static float alternativeBeautyModifier = -30f;
		public static bool replaceBoat = true;
		public static bool debug = false;

		public override void ExposeData()
		{
			Scribe_Values.Look<bool>(ref replaceTrashCanComp, "replaceTrashCanComp", true, false);
			Scribe_Values.Look<int>(ref trashCleanLimit, "trashCleanLimit", 8, false);
			Scribe_Values.Look<int>(ref trashLimit, "trashLimit", 10, false);
			Scribe_Values.Look<bool>(ref drawTrashBar, "drawTrashBar", true, false);
			Scribe_Values.Look<bool>(ref useAlternativeTrashBeauty, "useAlternativeTrashBeauty", false, false);
			Scribe_Values.Look<float>(ref alternativeBeautyModifier, "alternativeBeautyModifier", -30f, false);
			Scribe_Values.Look<bool>(ref replaceBoat, "replaceBoat", true, false);
			Scribe_Values.Look<bool>(ref debug, "debug", false, false);
			base.ExposeData();
		}

		public static void DrawSettings(Rect rect)
		{
			Listing_Standard listing_Standard = new Listing_Standard(GameFont.Small);
			listing_Standard.ColumnWidth = rect.width - 30f;
			listing_Standard.Begin(rect);
			listing_Standard.Gap();
			listing_Standard.CheckboxLabeled("Debug", ref debug, "Debug output. It's highly recommended that this is turned off.");
			listing_Standard.Gap();

			listing_Standard.CheckboxLabeled("Replace trash can comp", ref replaceTrashCanComp, "Turning this off will mean trash cans do nothing.");
			listing_Standard.Gap();
			if (replaceTrashCanComp)
			{
				listing_Standard.CheckboxLabeled("Trash can fill bar", ref drawTrashBar, "Fill bar that renders over trash cans. Can be ugly.");
				listing_Standard.Gap();

				listing_Standard.CheckboxLabeled("Use alternative trash can full beauty", ref useAlternativeTrashBeauty, "When trash cans are full, use a modified beauty modifier instead.");
				listing_Standard.Gap();

				if(useAlternativeTrashBeauty)
				{
					string alternativeBeautyModifierBuffer = alternativeBeautyModifier.ToString();
					listing_Standard.TextFieldNumericLabeled<float>("Alternative beauty modifier (default -30) ", ref alternativeBeautyModifier,ref alternativeBeautyModifierBuffer, float.MinValue, 1E+09f);
					listing_Standard.Gap();
				}

				string trashCleanLimitBuffer = trashCleanLimit.ToString();
				listing_Standard.TextFieldNumericLabeled<int>("Minimum cleaning limit (Default 8) ", ref trashCleanLimit,ref trashCleanLimitBuffer, 0f, 1E+09f);
				listing_Standard.Gap();

				string trashLimitBuffer = trashLimit.ToString();
				listing_Standard.TextFieldNumericLabeled<int>("Maximum trash can storage (Default 10) ", ref trashLimit,ref trashLimitBuffer, 0f, 1E+09f);
				listing_Standard.Gap();

				
			}

			listing_Standard.CheckboxLabeled("Replace boat building", ref replaceBoat, "Turning this off will mean boats do not animate.");
			listing_Standard.Gap();
			/*
				Rect rect2 = listing_Standard.GetRect(Text.LineHeight);
				Rect rect3 = rect2.LeftPartPixels(300f);
				Rect rect4 = rect2.RightPartPixels(rect2.width - 300f);
				Widgets.Label(rect3, "Length of day multiplier");
				int num = Mathf.RoundToInt(TimeControlSettings.speedMultiplier * 100f);
				num = Mathf.RoundToInt(Widgets.HorizontalSlider(rect4, (float)num, (float)TimeControlSettings.min, (float)TimeControlSettings.max, false, num.ToString() + "%", null, null, 1f));
				TimeControlSettings.speedMultiplier = (float)num / 100f;
				listing_Standard.Gap(12f);
				listing_Standard.CheckboxLabeled("Keep work speed relatively the same per day", ref TimeControlSettings.slowWork, "Disabling this will allow your colonists to get more work done in each day (if you make days longer), at the cost of making the game easier.");
				listing_Standard.Gap(12f);
				listing_Standard.CheckboxLabeled("Show Advanced (Use at your own risk)", ref TimeControlSettings.showAdvanced, "Use at your own risk.");
				listing_Standard.Gap(12f);
				if (TimeControlSettings.showAdvanced)
				{
					listing_Standard.CheckboxLabeled("Don't scale anything (not recommended)", ref TimeControlSettings.dontScale, "Don't scale anything. This basically just makes this mod a custom game speed controller.");
					listing_Standard.Gap(12f);
					if (!TimeControlSettings.dontScale)
					{
						listing_Standard.CheckboxLabeled("Scale pawns with length of day (recommended)", ref TimeControlSettings.scalePawns, "This will make sure your colonists eat, sleep, mine, craft, and so on at the normal rate per day of the base game. Disabling may cause weird (but potentially interesting?) things to happen. You've been warned.");
					}
					else
					{
						listing_Standard.Label("Scale pawns with length of day (recommended)", -1f, null);
					}
					listing_Standard.Gap(12f);
					listing_Standard.CheckboxLabeled("Unlock speed slider", ref TimeControlSettings.speedUnlocked, "This is probably a bad idea.");
					listing_Standard.Gap(12f);
					if (TimeControlSettings.speedUnlocked)
					{
						listing_Standard.CheckboxLabeled("Really? Are you sure? Extreme settings will cause... strange things to happen", ref TimeControlSettings.speedReallyUnlocked, "You've been warned.");
					}
				}
				listing_Standard.Gap(12f);
			}
			listing_Standard.CheckboxLabeled("Enable twelfth editing (requires restart):", ref TimeControlSettings.patchTwelfths, "Requires restart.");
			listing_Standard.Gap(12f);
			if (TimeControlSettings.patchTwelfths)
			{
				listing_Standard.TextFieldNumericLabeled<int>("Days per twelfth", ref TimeControlSettings.daysPerTwelfth, ref TimeControlSettings.daysPerTwelfthBuf, 0f, 2.1474836E+09f);
				listing_Standard.CheckboxLabeled("Enable debug logging:", ref TimeControlSettings.debug, "Not necessary for users.");
				if (TimeControlSettings.debug && Find.CurrentMap != null)
				{
					listing_Standard.LabelDouble("GenTemperature OffsetFromSeasonCycle(60000, 1)", GenTemperature.OffsetFromSeasonCycle(60000, 1).ToString(), null);
					int ticksGame = Find.TickManager.TicksGame;
					int tile = Find.CurrentMap.Tile;
					listing_Standard.LabelDouble(string.Concat(new object[]
					{
						"GenTemperature OffsetFromSeasonCycle(",
						ticksGame,
						",",
						tile,
						")"
					}), GenTemperature.OffsetFromSeasonCycle(ticksGame, tile).ToString(), null);
				}
			}
			*/
			listing_Standard.Gap();
			listing_Standard.End();
		}
	}
}
